FROM ubuntu:18.04
MAINTAINER Ondřej Čertík <ondrej@certik.us>

# Get sudo working
RUN apt-get update && DEBIAN_FRONTEND=noninteractive \
    apt-get install -yq --no-install-recommends \
        sudo ca-certificates \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && echo "swuser ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/swuser \
    && echo "Defaults env_reset" >> /etc/sudoers.d/swuser \
    && echo "Defaults env_keep = \"http_proxy https_proxy no_proxy\"" >> /etc/sudoers.d/swuser \
    && hash -r

# Switch to a user:
RUN groupadd -r swuser -g 1000 && \
    mkdir /home/swuser && \
    useradd -u 1000 -r -g swuser -d /home/swuser -s /sbin/nologin \
         -c "Docker image user" swuser && \
    chown -R swuser:swuser /home/swuser && \
    echo "swuser:swuser" | chpasswd && \
    adduser swuser sudo
WORKDIR /home/swuser
USER swuser


# Install Conda and required packages
RUN sudo apt-get update && DEBIAN_FRONTEND=noninteractive \
    sudo apt-get install -yq --no-install-recommends \
        wget bzip2 gcc gcc-multilib cmake make git && \
    sudo apt-get clean && \
    sudo rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* && \
    wget --no-check-certificate https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh -O miniconda.sh && \
    bash miniconda.sh -b -p $HOME/conda_root && \
    rm miniconda.sh && \
    export PATH="$HOME/conda_root/bin:$PATH" && \
    conda config --set always_yes yes --set changeps1 no && \
    conda info -a && \
    conda update -q -n root conda && \
    conda clean --all && \
    hash -r

# Test our binder files
COPY --chown=swuser:swuser binder binder
COPY --chown=swuser:swuser test_binder.sh test_binder.sh
COPY --chown=swuser:swuser Demo.ipynb Demo.ipynb
COPY --chown=swuser:swuser lfortran_version lfortran_version
RUN export PATH="$HOME/conda_root/bin:$PATH" && \
    ./test_binder.sh
