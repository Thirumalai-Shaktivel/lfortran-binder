#! /bin/bash

set -ex

conda env create -f binder/environment.yml
conda clean --all

set +x
. $(conda info --root)/etc/profile.d/conda.sh
conda activate lfortran
set -x

binder/postBuild
